package com.mani.lookupApplication.model;

public enum Role {
    USER, ADMIN
}
