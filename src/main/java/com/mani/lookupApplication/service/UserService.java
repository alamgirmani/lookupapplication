package com.mani.lookupApplication.service;

import com.mani.lookupApplication.model.PhoneBook;
import com.mani.lookupApplication.model.Role;
import com.mani.lookupApplication.model.User;
import com.mani.lookupApplication.repository.PhoneBookRepository;
import com.mani.lookupApplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PhoneBookRepository phoneBookRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PhoneBookRepository phoneBookRepository, BCryptPasswordEncoder bCryptPasswordEncoder){
        this.userRepository = userRepository;
        this.phoneBookRepository = phoneBookRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void addUser(User user){
        String randomId = user.getFirstName().substring(0,2) +
                user.getLastName().substring(0,2) +
                UUID.randomUUID().toString();
        user.setTuId(randomId.substring(0,8));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole(Role.USER);
        userRepository.save(user);
        PhoneBook phoneBook = new PhoneBook(user.getFirstName() + " " + user.getLastName(), user.getPhoneNumber());
        phoneBookRepository.save(phoneBook);
    }

    public List<User> getUsers(){
        return userRepository.findAll();
    }

    public void updateUser(User user){
        userRepository.save(user);
    }

    public void deleteUser(User user){
        userRepository.delete(user);
    }

    public User getUserById(Long id) {
        Optional<User> optional = userRepository.findById(id);
        User user;
        if(optional.isPresent()){
            user = optional.get();
        }else {
            throw new RuntimeException("User not found for id " + id);
        }
        return user;
    }

    public String getUserName(String search) {
        return userRepository.getUserName(search);
    }

    @Override
    public UserDetails loadUserByUsername(String tuId) throws UsernameNotFoundException {
        return userRepository.findByTuId(tuId);
    }
}
