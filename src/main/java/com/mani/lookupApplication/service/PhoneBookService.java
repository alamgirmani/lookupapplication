package com.mani.lookupApplication.service;

import com.mani.lookupApplication.model.PhoneBook;
import com.mani.lookupApplication.repository.PhoneBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneBookService {

    private final PhoneBookRepository phoneBookRepository;

    @Autowired
    public PhoneBookService(PhoneBookRepository phoneBookRepository){
        this.phoneBookRepository = phoneBookRepository;
    }
    public List<PhoneBook> getPhoneBook(){
        return phoneBookRepository.findAll();
    }

    public List<PhoneBook> getPhoneBookForUser(String name) {
        return phoneBookRepository.findByName(name);
    }
}
