package com.mani.lookupApplication.repository;

import com.mani.lookupApplication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT concat(first_name, ' ', last_name) as name " +
            "FROM lookup_application.user " +
            "WHERE MATCH (first_name,last_name,tu_id) " +
            "AGAINST (:search IN NATURAL LANGUAGE MODE)", nativeQuery = true)
    String getUserName(@Param("search") String search);

    User findByTuId(String tuId);
}
