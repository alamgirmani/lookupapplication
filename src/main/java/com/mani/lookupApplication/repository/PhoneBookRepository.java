package com.mani.lookupApplication.repository;

import com.mani.lookupApplication.model.PhoneBook;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneBookRepository extends MongoRepository<PhoneBook, String> {
    List<PhoneBook> findByName(String name);
}
