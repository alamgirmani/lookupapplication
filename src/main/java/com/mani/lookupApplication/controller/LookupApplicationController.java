package com.mani.lookupApplication.controller;

import com.mani.lookupApplication.model.PhoneBook;
import com.mani.lookupApplication.model.User;
import com.mani.lookupApplication.service.UserService;
import com.mani.lookupApplication.service.PhoneBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/lookupApplication")
public class LookupApplicationController {
    private final UserService userService;
    private final PhoneBookService phoneBookService;

    @Autowired
    public LookupApplicationController(UserService userService, PhoneBookService phoneBookService){
        this.userService = userService;
        this.phoneBookService = phoneBookService;
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }

    @GetMapping( "/login")
    public String login(){
        return "login";
    }

    @PostMapping("/logout")
    public String logout(){
        return "login";
    }

    @GetMapping("/admin/users")
    public String getUsers(Model model){
        List<User> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping ("/admin/addUser")
    public String addUser(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "add-user";
    }

    @PostMapping ("/admin/addUser")
    public String addUser(@ModelAttribute("user") User user, Model model){
        userService.addUser(user);
        List<User> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/update/{id}")
    public String updateUser(@PathVariable Long id, Model model){
        User updatedUser = userService.getUserById(id);
        model.addAttribute("updatedUser", updatedUser);
        return "update-user";
    }

    @PostMapping("/update")
    public String updateUser(@ModelAttribute("updatedUser")User updatedUser, Model model){
        userService.updateUser(updatedUser);
        List<User> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/admin/delete/{id}")
    public String deleteUser(@PathVariable Long id, Model model){
        userService.deleteUser(userService.getUserById(id));
        List<User> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/admin/phoneBook")
    public String getPhoneBook(Model model){
        model.addAttribute("phoneBookList", phoneBookService.getPhoneBook());
        return "phone-book";
    }

    @PostMapping("/phoneBookForUser")
    public String getPhoneBookForUser(@RequestParam String search, Model model){
        String name = userService.getUserName(search);
        List<PhoneBook> phoneBookList = phoneBookService.getPhoneBookForUser(name);
        model.addAttribute("phoneBookList", phoneBookList);
        return "phone-book-for-user";
    }
    
}
